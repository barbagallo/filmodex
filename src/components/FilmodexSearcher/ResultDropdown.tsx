import React, { FunctionComponent } from "react";
import styled from "styled-components";
import { ImpulseSpinner } from "react-spinners-kit";

export interface IResultDropdownProps {
  open: boolean;
  isFetching: boolean;
  results: null | any[];
  onSelect: any;
}

const SearchResults = styled.div<{ open: boolean }>`
  display: ${({ open }) => (open ? "block" : "none")};
  background: #e2e2e2;
  padding: 10px;
  position: absolute;
  width: 100%;
  top: 35px;
`;

const SearchResultItem = styled.div`
  padding: 12px;
  margin: 5px 0;
  &:hover {
    background: #efefef;
    cursor: pointer;
  }
`;

const SpinnerWrap = styled.div`
  padding: 20px;
`;

const ResultDropdown: FunctionComponent<IResultDropdownProps> = (props) => {
  const { open, results, onSelect, isFetching } = props;

  if (isFetching) {
    return (
      <SearchResults data-testid="loading-results" open={true}>
        <SpinnerWrap>
          <ImpulseSpinner size={50} loading={true} />
        </SpinnerWrap>
      </SearchResults>
    );
  }

  if (!results || results.length === 0) {
    return null;
  }

  const resultList = results.map((result) => {
    return (
      <SearchResultItem
        onClick={() => {
          onSelect(result);
        }}
        key={result.imdbID}
      >
        {result.Title}
      </SearchResultItem>
    );
  });

  return (
    <SearchResults data-test-id="final-results" open={open}>
      {resultList}
    </SearchResults>
  );
};

export default ResultDropdown;
