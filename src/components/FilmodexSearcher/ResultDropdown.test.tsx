import React from "react";
import { render, screen } from "@testing-library/react";
import ResultDropdown from "./ResultDropdown";
import { ImpulseSpinner } from "react-spinners-kit";
import { IOmbdResult } from "./FilmodexSearcher";

const mockResults: IOmbdResult[] = [
  {
    Title: "Back to the Future",
    Year: "1985",
    imdbID: "tt0088763",
    Type: "movie",
    Poster:
      "https://m.media-amazon.com/images/M/MV5BZmU0M2Y1OGUtZjIxNi00ZjBkLTg1MjgtOWIyNThiZWIwYjRiXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg",
  },
  {
    Title: "Back to the Future Part II",
    Year: "1989",
    imdbID: "tt0096874",
    Type: "movie",
    Poster:
      "https://m.media-amazon.com/images/M/MV5BZTMxMGM5MjItNDJhNy00MWI2LWJlZWMtOWFhMjI5ZTQwMWM3XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg",
  },
  {
    Title: "Back to the Future Part III",
    Year: "1990",
    imdbID: "tt0099088",
    Type: "movie",
    Poster:
      "https://m.media-amazon.com/images/M/MV5BYjhlMGYxNmMtOWFmMi00Y2M2LWE5NWYtZTdlMDRlMGEzMDA3XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg",
  },
];

test("renders an open dropdown with items", () => {
  render(
    <ResultDropdown
      open={true}
      isFetching={false}
      results={mockResults}
      onSelect={jest.fn()}
    />
  );
  const expectedTitle = screen.getByText(mockResults[2].Title);
  expect(expectedTitle).toBeInTheDocument();
});

test("renders closed even if supplied with results", () => {
  render(
    <ResultDropdown
      open={false}
      isFetching={false}
      results={mockResults}
      onSelect={jest.fn()}
    />
  );
  const expectedTitle = screen.getByText(mockResults[2].Title);
  expect(expectedTitle).not.toBeVisible();
});

test("displays loader when fetching data", () => {
  render(
    <ResultDropdown
      open={false}
      isFetching={true}
      results={[]}
      onSelect={jest.fn()}
    />
  );
  const expectedTitle = screen.getByTestId("loading-results");
  expect(expectedTitle).toBeVisible();
});

test("hides dropdown when data list is empty", async () => {
  render(
    <ResultDropdown
      open={true}
      isFetching={true}
      results={[]}
      onSelect={jest.fn()}
    />
  );
  const expectedTitle = screen.queryByTestId("final-results");
  expect(expectedTitle).toBe(null);
});

test("hides dropdown when data list is null", async () => {
  render(
    <ResultDropdown
      open={true}
      isFetching={true}
      results={null}
      onSelect={jest.fn()}
    />
  );
  const expectedTitle = screen.queryByTestId("final-results");
  expect(expectedTitle).toBe(null);
});
