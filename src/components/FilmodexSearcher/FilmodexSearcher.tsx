import React, { useCallback, useState } from "react";
import styled from "styled-components";
import { Input, Button, Col, Row, InputGroup } from "reactstrap";
import ResultDropdown from "./ResultDropdown";

interface IOmdbResults {
  Search: IOmbdResult[];
  Response: boolean;
  totalResults: number;
}

export interface IOmbdResult {
  Title: string;
  Year: string;
  imdbID: string;
  Type: string;
  Poster: string;
}

const ResultCounter = styled.div`
  color: #333;
  padding: 5px 0 0 0;
`;

const MovieInfo = styled.div`
  margin: 20px 0;
  .poster {
    margin-bottom: 15px;
  }
`;

const FilmodexSearcher = (props: any) => {
  const [omdbData, updateOmdbData] = useState<IOmdbResults>({
    Search: [],
    Response: false,
    totalResults: 0,
  });

  const [showSearchDD, updateShowSearchDD] = useState<boolean>(false);

  const [isFetching, updateIsFetching] = useState<boolean>(false);

  const [searchQuery, updateSearchQuery] = useState<string>("");

  const [selectedFilm, updateSelectedFilm] = useState<IOmbdResult | null>(null);

  const doSearch = useCallback(() => {
    // reset selected film during search
    updateSelectedFilm(null);
    updateIsFetching(true);
    fetch(`https://www.omdbapi.com/?s=${searchQuery}&apikey=902755be`)
      .then((res) => res.json())
      .then((res) => {
        updateOmdbData(res);
        updateShowSearchDD(true);
        updateIsFetching(false);
      });
  }, [searchQuery, updateOmdbData]);

  const onSearchInput = useCallback(
    (e) => {
      updateSearchQuery(e.currentTarget.value);
    },
    [updateSearchQuery]
  );

  const onSearchKeyDown = useCallback(
    (e) => {
      if (e.keyCode === 13 && !isFetching) {
        doSearch();
      }
    },
    [doSearch, isFetching]
  );

  const onSelectResult = useCallback(
    (result: IOmbdResult) => {
      // hide results and reset the search query
      updateShowSearchDD(false);
      updateSearchQuery("");
      updateSelectedFilm(result);
    },
    [updateShowSearchDD]
  );

  return (
    <div>
      <Row>
        <Col md="8" xs="8">
          <InputGroup>
            <Input
              onChange={onSearchInput}
              onKeyDown={onSearchKeyDown}
              placeholder="Enter a movie title"
              value={searchQuery}
            />
            <ResultDropdown
              open={showSearchDD}
              results={omdbData.Search}
              onSelect={onSelectResult}
              isFetching={isFetching}
            />
            {/* TODO (enhancement) replace above w/ autocomplete + debounce */}
          </InputGroup>
        </Col>
        <Col lg="1">
          <Button
            disabled={isFetching || searchQuery.length === 0}
            onClick={doSearch}
          >
            Search
          </Button>
        </Col>
        <Col>
          <ResultCounter>
            {omdbData.Response &&
              (omdbData.totalResults > 0
                ? `${omdbData.totalResults} results`
                : "No films found.")}
          </ResultCounter>
        </Col>
      </Row>

      <Row>
        <Col lg="4" md="4" sm="12">
          {selectedFilm && (
            <MovieInfo>
              <img
                className="poster"
                alt={selectedFilm.Title}
                src={selectedFilm.Poster}
                width="100%"
              />
              <h3>{selectedFilm.Title}</h3>
              <h4>{selectedFilm.Year}</h4>
            </MovieInfo>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default FilmodexSearcher;
