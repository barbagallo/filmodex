import React from "react";
import { Navbar, NavbarBrand } from "reactstrap";

const AppHeader = (props: any) => {
  return (
    <header>
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/">filmodex</NavbarBrand>
      </Navbar>
    </header>
  );
};

export default AppHeader;
