import React from "react";
import AppHeader from "./components/AppHeader";
import FilmodexSearcher from "./components/FilmodexSearcher";
import { Container, Row, Col } from "reactstrap";

function App() {
  return (
    <Container>
      {/* Header */}
      <Row>
        <Col>
          <AppHeader />
        </Col>
      </Row>

      {/* Content */}
      <Row>
        <Col>
          <FilmodexSearcher />
        </Col>
      </Row>

      {/* Footer */}
      <Row>
        <Col>
          <footer>
            &copy; {new Date().getFullYear()} filmodex inc. - All Right
            Reserved, n' stuff. Uses{" "}
            <a href="https://www.omdbapi.com/">OMDBAPI</a>.
          </footer>
        </Col>
      </Row>
    </Container>
  );
}

export default App;
