import React from "react";
import { render, screen } from "@testing-library/react";
import App from "./App";

test("renders search input and button", () => {
  render(<App />);
  const searchInput = screen.getByPlaceholderText(/Enter a movie title/i);
  const searchBtn = screen.getByText(/Search/i);
  expect(searchInput).toBeInTheDocument();
  expect(searchBtn).toBeInTheDocument();
});
